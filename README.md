# poly_fft

Polynomial multiplication using Fast Fourier Transform. O(n log(n)) instead of O(n squared)
