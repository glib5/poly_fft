import numpy as np
import matplotlib.pyplot as plt


def adj_poly(p1, p2):
    '''takes 2 arrays returns them as 2 arrays where
    len(array) is a power of 2 (the min 2^n such that
    2^n >= max(len1, len2) ). Higher orders filled with 0'''
    L = max(len(p1), len(p2))
    i = 0
    while 1:
        LL = 1<<i
        if LL>=L:
            break
        else:
            i += 1
    P1 = np.concatenate((p1, np.zeros(shape=LL - len(p1))))
    P2 = np.concatenate((p2, np.zeros(shape=LL - len(p2))))
    return P1, P2 # same len


def fft_poly_mult(p1, p2):
    '''polynomials multiplication using FFT. p1, p2 are coeff repr'''
    P1, P2 = adj_poly(p1, p2) # calls above function
    z = np.zeros(shape=len(P1)) # lenP1==lenP2
    pp1 = np.concatenate((P1, z))
    pp2 = np.concatenate((P2, z))
    v1 = np.fft.fft(pp1) # value repr
    v2 = np.fft.fft(pp2)
    # coeff repr of the solution (imag part and higher order coeffs are always 0, discarded)
    return np.real(np.fft.ifft(v1*v2))[:(len(p1)+len(p2)-1)] 


def eval_poly(P, x):
    '''evaluates a polynomial P (coeff repr) at x
    P[1, 2, 3] = 1 + 2x + 3x^2 (Horner method)'''
    y = np.full(shape=len(x), fill_value=P[0], dtype=np.double)
    X = np.copy(x)
    for i in range(1, len(P)):
        if P[i]!=0:
            y += P[i]*X
        X *= x 
    return y


#---------------------------------------------

def myplot(p1, p2, p):
    a = 2.1
    x = np.arange(-a, a, .05)
    # evaluate a, b, c=a*b
    x1 = eval_poly(p1, x)
    x2 = eval_poly(p2, x)
    y = x1*x2
    pfft = eval_poly(p, x)
    plt.figure()
    plt.plot(x, x1, label="p1", linewidth=.8)
    plt.plot(x, x2, label="p2", linewidth=.8)
    plt.plot(x, y, label="prod=p1*p2", color="r")
    plt.plot(x, pfft, label="fft for prod", linestyle="--", color="k")
    plt.axvline(color="k", linewidth=.5)
    plt.axhline(color="k", linewidth=.5)
    plt.legend()
    plt.grid()
    plt.show()


def ex1():
    p1 = [2, -1]
    p2 = [0, 1, -2]
    p1 = np.asarray(p1)
    p2 = np.asarray(p2) 
    # compute c = a*b with fft
    p = fft_poly_mult(p1, p2)
    print("p1: ", p1)
    print("p2: ", p2)
    print("prod: ", p)
    myplot(p1, p2, p)


def ex2():
    print("\nFirst polynomial")
    p1 = []
    while 1:
        i = input("Insert coeff, 'w' to end. ")
        if i=='w':
            break
        else:
            p1.append(int(i))
    print("\nSecond polynomial")
    p2 = []
    while 1:
        i = input("Insert coeff, 'w' to end. ")
        if i=='w':
            break
        else:
            p2.append(int(i))
    p1 = np.asarray(p1)
    p2 = np.asarray(p2)
    
    # compute c = a*b with fft
    p = fft_poly_mult(p1, p2)

    
    print("\np1: ", p1)
    print("p2: ", p2)
    print("prod: ", p)
    myplot(p1, p2, p) 


#--- MAIN ---#

if __name__=="__main__":

    ex1()

##    ex2()